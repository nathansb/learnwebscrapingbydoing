#Ben Nathanson
#12.29.2017
#new: 
# fixed 1: create methods to handle a repeat search story
# fixed 2: encapsulated methods to improve reading and debugging
# feature request: create search URL from raw input
# fix request 1 : refactor the comparison/print loop
# fix request 2 : check that no value of a result is null before printing the result 
# using eBay API a special URL string
from urllib.request import urlopen
from bs4 import BeautifulSoup
from urllib.error import HTTPError, URLError
import math, time, datetime

def bayScrape(ebay_target_url, exclusion_terms, fileName):
	total_prices = 0
	prices_list=[]
	errors_list=""
	final_results=[]
	prices_variance_sum = 0
	
	#get the ebay results:
	try:
		page_request = urlopen(ebay_target_url)
	except HTTPError as e:
		print(e)
		print("HTTP error.")
		page_request.close()
		exit
	except URLError as e:
		print(e)
		print("URL error.")
		page_request.close()
		exit
	else:
		print("Success! Retrieving HTML. \n")
		
	#read the page
	try:
		raw_html_import = page_request.read()
	except NameError as e:
		print(e)
		print("Name error.")
		page_request.close()
		exit
	except AttributeError as e:
		print(e)
		print("Attribute error.")
		page_request.close()
		exit
	
	page_request.close()
	timestamp = time.time()
	timestampformatted = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
	page_soup = BeautifulSoup(raw_html_import, "html.parser")
	list_elements_found = page_soup.findAll("li",{"class":"sresult"})
	
	#filter the results and build a model of the price curve
	for element in list_elements_found :
		title = element.a.img["alt"].replace(",","").upper()
		uid = element["listingid"]
		itemRef = element.a["href"]
		has_excluded_word = False
		for exclusion_term in exclusion_terms:
			if exclusion_term in title:
				has_excluded_word = True
		if (has_excluded_word != True):
			item_price = element.span.text.replace("\n","").replace("\t","").replace(",","").replace("$","")
			try: 
				float_item_price = float(item_price)				
				prices_list.append(float_item_price)
				total_prices += float_item_price
				final_results.append([uid, title, float_item_price, itemRef, timestampformatted])
			except:
				errors_list += "We could not get a price for one or more items. \n"
				
	#calculate a curve and remove things outside the bounds
	list_length = len(prices_list)
	if list_length != 0: 
		prices_mean = total_prices / list_length
	count = 0
	while count < list_length:
		prices_variance_sum += math.fabs((float(prices_list[count])-prices_mean)**2)
		count += 1
	prices_stdev = math.sqrt((prices_variance_sum)/(list_length-1))
	average_output = prices_mean - (prices_mean % 1)
	band_tightness = .60
	lowerBound = average_output - (prices_stdev * band_tightness)
	upperBound = average_output + (prices_stdev * band_tightness)
	rejected = 0	
	best_deal = final_results[0][2] 

	#print out the filtered results and explain how the filter performed
	j = 0
	while j < len(final_results):
		if (lowerBound < final_results[j][2] < upperBound):
			print("ID: " + final_results[j][0] + "\n"
					+ final_results[j][1].lower().title() +  "\n"
					+ "Price: $" + str(final_results[j][2]) + "\n"
					+ "Link: " + final_results[j][3]
					+ "\n")
			if final_results[j][2] < best_deal:
				best_deal = final_results[j][2]
			else:
				pass
		else:
			rejected += 1
		j += 1

	print("Best Deal: $" + str(best_deal))	
	print("We rejected " + str(rejected) + " results out of " 
		+ str(len(prices_list)))
	print("Time received: " + datetime.datetime.fromtimestamp(timestamp).strftime('%A, %B %-d, %Y at %-I:%M %p'))
	fileName += ".csv"
	fileEdit = open(fileName, "w")
	print("Created a new file: " + fileName)
	columnHeadings = "ID, Title, Price, Hyperlink, Time Received"
	fileEdit.write(columnHeadings + "\n")
	result = 0
	while result < len(final_results): 
		cycle = 0
		#refactor comparison/loop methods
		while cycle < 5:
			try:
				if (lowerBound < final_results[result][2] < upperBound):
					fileEdit.write(str(final_results[result][cycle]) + ",")
				cycle += 1
			except:
				errors_list += "We could not print this result to a csv. \n"
				cycle += 1
		if (lowerBound < final_results[result][2] < upperBound):		
			fileEdit.write("\n")
		result += 1
	if len(errors_list) > 0:
		print("Errors: " + errors_list)

	print("Finished creating csv output.")
	fileEdit.close()
	
def request(times_repeated):
	urlinput = 'https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2380057.m570.l1313.TR11.TRC2.A0.H0.X.TRS1&_nkw=essential+PH-1&_sacat=0'
	badWords = ['SPRINT','FOR PARTS']
	fileName = "myeBaySearch" + str(times_repeated)
	bayScrape(urlinput, badWords, fileName)
	
def userInput(delay, repeats):	
	try:
		i = 0 
		while i < int(repeats):
			i+=1
			request(i)
			time.sleep(int(delay) * 60)
	except:
		print("It appears that your input was not accepted. Try again with two whole numbers. ")
		exit

print("Thank you for using eBayScraper. ")
wait_time = int(input('In hours, how often do you want to perform your search?\n'))
how_many_times = int(input('How many times shall we repeat your search?\n'))
userInput(wait_time, how_many_times)